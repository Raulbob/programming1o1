package com.brfpfa;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
//        Animal myAnimal = new Animal(); //nu poti instantia o clasa abstracta

        Animal animal = new Animal("cocor", 1) {

            @Override
            public void talk() {
                System.out.println("Cra cra!!");
            }

        }; //anonymous class


        Animal animal1 = new Caine("rex", 6);
        Animal animal2 = new Pisica("vanessa", 2);

        List<Animal> animale = new ArrayList<>();
        animale.add(animal);
        animale.add(animal1);
        animale.add(animal2);

        faGalagie(animale);
    }

    private static void faGalagie(List<Animal> animale) {
        animale.forEach(animal -> animal.talk());
    }

}
