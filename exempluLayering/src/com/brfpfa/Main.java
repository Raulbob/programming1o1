package com.brfpfa;

import com.brfpfa.controller.Controller;
import com.brfpfa.model.Animal;
import com.brfpfa.model.Caine;
import com.brfpfa.model.Pisica;
import com.brfpfa.repository.InMemoryRepository;
import com.brfpfa.repository.Repository;
import com.brfpfa.view.View;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Repository repository = new InMemoryRepository();
        Controller controller = new Controller(repository);
        View view = new View(controller);

        view.start();
    }

}
