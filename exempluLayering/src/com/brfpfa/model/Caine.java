package com.brfpfa.model;

import com.brfpfa.model.Animal;

public class Caine extends Animal {

    public Caine(String name, int age) {
        super(name, age);
    }

    @Override
    public void talk() {
        System.out.println("Ham ham!");
    }

    public String toString() {
        return "Caine: " + getName() + " " + getAge();
    }

}
