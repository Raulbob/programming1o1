package com.brfpfa.model;

import com.brfpfa.model.Animal;

public class Pisica extends Animal {


    public Pisica(String name, int age) {
        super(name, age);
    }

    @Override
    public void talk() {
        System.out.println("Miau miau!!");

    }

    public String toString() {
        return "Pisica: " + getName() + " " + getAge();
    }

}
