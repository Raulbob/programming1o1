package com.brfpfa.controller;

import com.brfpfa.model.Animal;
import com.brfpfa.model.Caine;
import com.brfpfa.repository.InMemoryRepository;
import com.brfpfa.repository.Repository;

import java.util.ArrayList;
import java.util.List;

public class Controller {

    private Repository repository;

    public Controller(Repository inMemoryRepository) {
        this.repository = inMemoryRepository;
    }

    public List<Animal> findAllAnimals() {
        return new ArrayList<Animal>() {{
            addAll(repository.getCaini());
            addAll(repository.getPisici());
        }};
    }


    public void addNewCaine(String name, int age) {
        repository.addCaine(new Caine(name, age));
    }
}
