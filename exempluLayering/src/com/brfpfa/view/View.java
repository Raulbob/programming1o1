package com.brfpfa.view;

import com.brfpfa.controller.Controller;
import com.brfpfa.model.Animal;
import com.brfpfa.model.Caine;

import static com.brfpfa.util.InputOutput.*;

import java.util.Scanner;

public class View {

    private final Controller controller;

    public View(Controller controller) {
        this.controller = controller;
    }


    public void printAllAnimals() {
        for (Animal animal : controller.findAllAnimals()) {
            System.out.println(animal);
        }

    }

    public void start() {
        while (true) {
            System.out.println(buildMenu());
            int input = readInt();

            if (input == 0) {
                return;
            } else if (input == 1) {
                printAllAnimals();
            } else if (input == 2) {
                addCaine();
            } else {
                System.out.println("Unimplemented. Sry. \n");
            }
        }
    }

    private void addCaine() {
        print("Nume:");
        String name = readString();
        print("Varsta:");
        String age = readString();

        try {
            controller.addNewCaine(name, Integer.parseInt(age));
        } catch (NumberFormatException e) {
            print("Am zis VARSTA!!! Main incearca.");
        }
    }


    private String buildMenu() {
        return "---MENU---\n1. Printeaza animale; \n2. Adauga caine; \n0. Exit";
    }
}
