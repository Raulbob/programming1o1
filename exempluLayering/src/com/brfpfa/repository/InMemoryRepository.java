package com.brfpfa.repository;

import com.brfpfa.model.Animal;
import com.brfpfa.model.Caine;
import com.brfpfa.model.Pisica;

import java.util.ArrayList;
import java.util.List;

public class InMemoryRepository implements Repository {

    private List<Caine> caini = new ArrayList<Caine>() {{
        add(new Caine("rex", 6));
        add(new Caine("Dodo", 16));
    }};

    private List<Pisica> pisici = new ArrayList<Pisica>() {{
        add(new Pisica("maria", 2));
        add(new Pisica("pricipesa", 3));
    }};


    @Override
    public List<Caine> getCaini() {
        return caini;
    }

    public void setCaini(List<Caine> caini) {
        this.caini = caini;
    }

    @Override
    public List<Pisica> getPisici() {
        return pisici;
    }

    @Override
    public void addCaine(Caine caine) {
        caini.add(caine);
    }

    public void setPisici(List<Pisica> pisici) {
        this.pisici = pisici;
    }

}
