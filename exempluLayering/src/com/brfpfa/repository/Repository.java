package com.brfpfa.repository;

import com.brfpfa.model.Caine;
import com.brfpfa.model.Pisica;

import java.util.List;

public interface Repository {

    List<Caine> getCaini();

    List<Pisica> getPisici();

    void addCaine(Caine caine);
}

