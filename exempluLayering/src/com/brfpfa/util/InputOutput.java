package com.brfpfa.util;

import java.util.Scanner;

public class InputOutput {

    public static String readString() {
        /* This reads the input provided by user
         * using keyboard
         */
        Scanner scan = new Scanner(System.in);
        // This method reads the number provided using keyboard
        return scan.nextLine();
    }

    public static int readInt() {
        /* This reads the input provided by user
         * using keyboard
         */
        Scanner scan = new Scanner(System.in);
        // This method reads the number provided using keyboard
        return scan.nextInt();
    }

    public static void print(String string) {
        System.out.println(string);
    }

}
